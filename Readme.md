# Git Cleanup

Cleans up all remote deleted branches that exist locally.

## PreReqs
You need to have GNU Stow installed.

## Installation
Place the script `src/bin/git-cleanup` somewhere in your `$PATH`.

Or run `make` to place in `~/.local/bin/`

Or use the `TARGETDIR` variable to change the destination.
