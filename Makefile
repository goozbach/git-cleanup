TARGETDIR ?= ~/.local

.PHONY: all

all:
	stow -t $(TARGETDIR) --no-folding -R src
